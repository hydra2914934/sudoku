let cellIndex = 70; //10, 40, 70

let rowIndex = Math.floor(cellIndex / 9);
let columnIndex = cellIndex % 9;
let gridIndex =
  Math.floor(cellIndex / 27) * 3 + Math.floor((cellIndex % 9) / 3);

console.log(`Cell Index is ${cellIndex}`);
console.log(`Row Index is ${rowIndex}`);
console.log(`Column Index is ${columnIndex}`);
console.log(`Grid Index is ${gridIndex}`);

let cValue = 0;
let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
let indx = arr.indexOf(cValue);
console.log(`Index is ${indx}`);
if (indx != -1) {
  arr.splice(indx, 1);
}

console.log(arr);
